# Papers related to Optimization of Molecules via Deep Reinforcement Learning

#### Methods in Molecule Generation and Optimization Literature
- Genetic (Evolutionary) Algorithms
- Deep Generative Models
- Reinforcement Learning

### Molecule Representations
- Graph Representation
- SMILES String Representation

### To Read

- **Constrained Bayesian Optimization for Automatic Chemical Design**: This contribution presents an approach based on constrained Bayesian optimization to find a promising molecule for a given application and doesn't cover reinforcement learning at all.

- **CORE, Automatic Molecule Optimization Using Copy & Refine Strategy**: This paper proposes a graph based strategy which will decide whether to copy a substructure from the input molecule (Copy) or sample a novel substructure from the entire space of substructures (Refine). It doesn't cover reinforcement learning.

- **Direct Steering of de novo Molecular Generation using Descriptor Conditional Recurrent Neural Networks (cRNNs)**: In this paper, authors construct a conditional recurrent neural networks (cRNNs) by setting the internal states of long short-term memory cells (LSTM) as the input conditions. No RL, again.

- **EvoMol: a flexible and interpretable evolutionary algorithm for unbiased de novo molecular generation**: This paper presents EvoMol, a new generic and simple molecular generation method using an evolutionary algorithm to sequentially build molecular graphs. With a flavor of RL.

- **Molecular Optimization by Capturing Chemist's Intuition Using Deep Neural Networks**: In this paper The problem of molecular optimization is framed as a machine translation problem in natural language processing (NLP) which is build on SMILES representation of molecules. Multiple layers of Long short-term memory (LSTM) is used in the encoder and decoder. No RL.

- **Multi-Objective Molecule Generation using Interpretable Substructures**: This paper proposes a novel approach to multi-property molecular optimization using generative models and graph representation of the molecules. Model is fine-tuned using reinforcement learning.

- **Reinforcement Learning for Molecular Design Guided by Quantum Mechanics**: This work introduces novel RL formulation for general molecular design in Cartesian coordinates which an agent places atoms onto a 3D canvas. Reward function is designed based on the electronic energy, which is approximated via fast quantum-chemical calculations.

- **Scaffold-constrained molecular generation**: This model is build on the well-known SMILES-based Recurrent Neural Network (RNN) generative model. It also directly benefits from the associated reinforcement learning methods, allowing to design molecules optimized for different properties while exploring only the relevant chemical space.

- **The power of deep learning to ligand-based novel drug discovery**: This paper introduces all famous deep learning  models that could be helpful in drug discovery process. It doesn't present a new approach, it just admires deep learning!


### Read
- **[Deep Reinforcement Learning for Multiparameter Optimization in de novo Drug Design](https://bitbucket.org/HSILA/papers/raw/22ad4717a7e30545c4c6edee9f9a816413eef878/papers/Deep%20Reinforcement%20Learning%20for%20Multiparameter%20Optimization%20in%20de%20novo%20Drug%20Design.pdf)**: This paper present a fragment-based reinforcement learning approach based on an actor−critic model, for the generation of novel molecules with optimal properties. The actor and the critic are both modeled with bidirectional long short-term memory (LSTM) networks.

- **[Molecular Design in Synthetically Accessible Chemical Space via Deep Reinforcement Learning](https://bitbucket.org/HSILA/papers/raw/22ad4717a7e30545c4c6edee9f9a816413eef878/papers/Molecular%20Design%20in%20Synthetically%20Accessible%20Chemical%20Space%20via%20Deep%20Reinforcement%20Learning.pdf)**: This contribution proposes a novel Reinforcement Learning framework for molecular design in which an agent learns to directly optimize through a space of synthetically-accessible drug-like molecules. It defines the environment’s state transitions as sequences of chemical reactions, allowing us to address the common issue of synthetic accessibility.

- **[Guiding Deep Molecular Optimization with Genetic Exploration](https://bitbucket.org/HSILA/papers/raw/22ad4717a7e30545c4c6edee9f9a816413eef878/papers/Guiding%20Deep%20Molecular%20Optimization%20with%20Genetic%20Exploration.pdf)**: This work, proposes genetic expert-guided learning (GEGL), which is a novel framework for training a molecule-generating DNN guided with genetic exploration. GEGL can be seen as a reinforcement learning algorithm with a novel mechanism for additional explorations.

- **[Learning To Navigate The Synthetically Accessible Chemical Space Using Reinfrocement Learning](https://bitbucket.org/HSILA/papers/raw/22ad4717a7e30545c4c6edee9f9a816413eef878/papers/Learning%20To%20Navigate%20The%20Synthetically%20Accessible%20Chemical%20Space%20Using%20Reinforcement%20Learning.pdf)**: This Paper presents a forward synthesis model powered by reinforcement learning (RL) entitled Policy Gradient for Forward Synthesis (PGFS) that treats the generation of a molecular structure as a sequential decision process of selecting reactant molecules and reaction transformations in a linear synthetic sequence.

- **[A multi-objective deep reinforcement learning framework](https://bitbucket.org/HSILA/papers/raw/418409e1e444366d39488a5f70963681483b0255/papers/A%20multi-objective%20deep%20reinforcement%20learning%20framework.pdf)**: This paper introduces multi-objective deep reinforcement learning (MODRL) framework. It doesn't study molecules specifically.

- **[Structure prediction of surface reconstructions by deep reinforcement learning](https://bitbucket.org/HSILA/papers/raw/418409e1e444366d39488a5f70963681483b0255/papers/Structure%20prediction%20of%20surface%20reconstructions%20by%20deep%20reinforcement%20learning.pdf)**: Authors of this paper recently proposed a ML method combining computer vision and reinforcement learning, the atomistic structure learning algorithm (ASLA). It enables structure prediction of 2D materials and molecules without any prior training or knowledge. In this work they extend the ASLA method to 3D structures.

- **[Multiple-objective Reinforcement Learning for Inverse Design and Identification](https://bitbucket.org/HSILA/papers/raw/418409e1e444366d39488a5f70963681483b0255/papers/Multiple-objective%20Reinforcement%20Learning%20for%20Inverse%20Design%20and%20Identification.pdf)**: This work presents a multiple-objective RL-based generative model for molecule identification (Inverse Molecule Design) which trys to satisfy a large number of  objectives (>10).

- **[The power of deep learning to ligand-based novel drug discovery](https://bitbucket.org/HSILA/papers/raw/8466018e8b3ef295fe94ccb0d625b56657a7c2c1/papers/The%20power%20of%20deep%20learning%20to%20ligand-based%20novel%20drug%20discovery.pdf)**: This paper presents an overview of useful deep learning approaches in drug discovery process and their PROs and CONs.